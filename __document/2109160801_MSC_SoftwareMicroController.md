<h2>MSC - MotorStepperController</h2>

<h3>Software MicroController</h3>


<h4>Anfahr- und Abfahr- Rampen</h4>

Sei $N_{S}$ die Anzahl der insgesamt zu fahrenden Steps, $N_U$ die Anzahl der Steps der Anfahrrampe und $N_D$ die Anzahl der Steps der Abfahrrampe.

**Beziehungen**

$N_U$ gleich oder ungleich $N_D, N_U \in \mathbb{N_0}, N_D \in \mathbb{N_0}$

$\boxed{R = \dfrac{200 stp}{2 \pi}}$ : typische StepperMotor-Auflösung in Schrittzahl pro Volldrehung

$\boxed{f(t) = \dfrac{N_{stp}}{s}}$ : Frequenz der StepperPulse : Anzahl der Steps pro Sekunde
$\boxed{P(t) = \dfrac{1}{f(t)}}$ : Periode zwischen StepperPulsen
$\boxed{\omega(t) = 2 \pi f(t)}$ : Winkelgeschwindigkeit der Motorachse

Frequenz $f$ und damit Periode $P$ sind bei An- und Abfahrt zeitabhängig. Bei der Motoransteuerung mit der für die Verfahrstrecke maximalen Drehzahl($f_H , \omega_H$, HochRampe) bleibt die Motorfrequenz zeitlich konstant:


**Fall 1: UpRamp**
$f_{Umin}(= 0)$ : UpRampStartfrequenz ,
$f_{Umax}$ : Endfrequenz UpRamp ,
$f_{Umin}(= 0) < f_{Umax}$,
$f_U = f_U(t) , f_U \in [f_{Umin}, f_{Umax}]$



**Fall 2: HighRamp**
$f_H = f_{Umax}$


**Fall 3: DownRamp**
$f_{Dmax} = f_H = f_{Umax}$ : DownRamp Startfrequenz ,
$f_{Dmin}(= 0)$ : DownRamp Endfrequenz,
$f_{Dmax} > f_{Dmin} (= 0)$,
$f_D = f_D(t) , f_D \in [f_{Dmin}, f_{Dmax}]$


**Zeitlicher Verlauf der Step-Folge**

Fall 1: $\boxed{U_{stp} + D_{stp} <= N_{stp}}$



Fall 2: $\boxed{N_{stp} < U_{stp} + D_{stp}}$








<!--
<h1></h1>
<h2></h2>
<h3></h3>
<h4></h4>
<h5></h5>
<h6></h6>
<h7></h7>
<b></b>
<b></b>
<b></b>
<i></i>
<i></i>
<i></i>
<></>
<></>
<></>
<></>

\boxed{ }

$A{}_{i} {}_{j} {}_{k} {}^{i} {}^{j} {}^{k}$

$A{}_{i} {}^{j} {}_{k} {}_{i} {}^{j} {}_{k}$

<h2>Links</h2>

<a href="https://de.wikipedia.org/wiki/Tr%C3%A4gheitstensor">Wikipedia Trägheitstensor</a>

-->
