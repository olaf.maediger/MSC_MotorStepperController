<h2>MSC - MotorStepperController</h2>

<h3>Übersicht</h3>

<b>MC</b>(MicroController, typisch <b>ESP32UnoBoard</b>, <b>STM32F103/F407Board</b>) steuert/regelt über IOPorts <b>SMD</b>(StepperMotorDriver, typisch <b>TB6600</b> oder <b>DM542T</b>) einen <b>SM</b>(StepperMotor, typisch NEMA11, NEMA17, NEMA19).

Basis-Kommunikation zwischen MC und PC findet über die Serielle Schnittstelle statt.
Über ein PC-TerminalProgramm sendet/empfängt der Benutzer Text-Commands/Events/Infos zum/vom MicroController.

Zusätzliche PC-Software ermöglicht dem Benutzer eine <b>GUI</b>(GraphicUserInterface)-Kommunikation mit dem MicroController über Serielle/USB-Schnittstelle, in Erweiterung auch über Bluetooth, Lan, WirelessLan und Mqtt.

Randbedingungen Hardware:

* Steuersignale MC: Step und Direction
* Eingangssignale MC: CurrentSense
* Die Anzahl der anzusteuernden Motoren hängt von der Anzahl der zur Verfügung stehenden IOPins ab.
  Ein Motor benötigt (minimal) zwei Output- und einen Input-Pin.

Randbedingungen Software:

  * Verwendete Compiler für MicroController: <b>ArduinoIDE</b> und <b>VSCode</b>
  * Verwendete PC-Software(vorerst getestet unter Windows): <b>Python</b> und <b>VS2019</b>


<h3>Themen</h3>

<a href="2109160759_MSC_HardwareMicroController.md" target="_top">Hardware MicroController</a>

<a href="2109160801_MSC_SoftwareMicroController.md" target="_top">Software MicroController</a>

<a href="2109160803_MSC_SoftwarePersonalComputer.md" target="_top">Software PersonalComputer</a>






<!--
<h1></h1>
<h2></h2>
<h3></h3>
<h4></h4>
<h5></h5>
<h6></h6>
<h7></h7>
<b></b>
<b></b>
<b></b>
<i></i>
<i></i>
<i></i>
<></>
<></>
<></>
<></>

$A{}_{i} {}_{j} {}_{k} {}^{i} {}^{j} {}^{k}$

$A{}_{i} {}^{j} {}_{k} {}_{i} {}^{j} {}_{k}$

<h2>Links</h2>

<a href="https://de.wikipedia.org/wiki/Tr%C3%A4gheitstensor">Wikipedia Trägheitstensor</a>


<h2>MSC - Theorie</h2>

 <h3>Übersicht</h3>

 <b>MC</b>(MicroController, typisch <b>ESP32UnoBoard</b>, <b>STM32F103/F407Board</b>)<br>
 steuert/regelt über IOPorts <b>SMD</b>(StepperMotorDriver, typisch <b>TB6600</b> oder<br>
 <b>DM542T</b>) einen <b>SM</b>(StepperMotor, typisch NEMA11, NEMA17, NEMA19).<br>
 <br>
 Basis-Kommunikation zwischen MC und PC findet über die Serielle Schnittstelle statt.<br>
 Über ein PC-TerminalProgramm sendet/empfängt der Benutzer Text-Commands/Events/Infos<br>
 zum/vom MicroController.<br>
 <br>
 Zusätzliche PC-Software ermöglicht dem Benutzer eine <b>GUI</b>(GraphicUserInterface)-<br>
 Kommunikation mit dem MicroController über Serielle/USB-Schnittstelle, in Erweiterung<br>
 auch über Bluetooth, Lan, WirelessLan und Mqtt.<br>
 <br>
 <b>Randbedingungen Hardware</b><br>
 * Steuersignale MC: Step und Direction<br>
 * Eingangssignale MC: CurrentSense<br>
 * Die Anzahl der anzusteuernden Motoren hängt von der Anzahl der zur Verfügung stehenden IOPins ab.<br>
 Ein Motor benötigt (minimal) zwei Output- und einen Input-Pin.<br>
 <br>
 <b>Randbedingungen Software</b><br>
 * Verwendete Compiler für MicroController: <b>ArduinoIDE</b> und <b>VSCode</b><br>
 * Verwendete PC-Software(vorerst getestet unter Windows): <b>Python</b> und <b>VS2019</b><br>
 <br>
 <br>


-->
